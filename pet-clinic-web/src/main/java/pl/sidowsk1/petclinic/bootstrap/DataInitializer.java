package pl.sidowsk1.petclinic.bootstrap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.sidowsk1.petclinic.model.*;
import pl.sidowsk1.petclinic.services.*;

import java.time.LocalDate;

@Component
public class DataInitializer implements CommandLineRunner {

    private final OwnerService ownerService;
    private final VetService vetService;
    private final PetTypeService petTypeService;
    private final SpecialtyService specialtyService;
    private final VisitService visitService;

    public DataInitializer(OwnerService ownerService, VetService vetService, PetTypeService petTypeService,
                           SpecialtyService specialtyService, VisitService visitService) {
        this.ownerService = ownerService;
        this.vetService = vetService;
        this.petTypeService = petTypeService;
        this.specialtyService = specialtyService;
        this.visitService = visitService;
    }

    @Override
    public void run(String... args) throws Exception {

        int count = petTypeService.findAll().size();

        if (count == 0) {
            loadData();
        }

    }

    private void loadData() {
        PetType dog = new PetType();
        dog.setName("Dog");
        PetType saveDogPetType = petTypeService.save(dog);

        PetType cat = new PetType();
        cat.setName("Cat");
        PetType saveCatPetType = petTypeService.save(cat);

        Specialty radiology = new Specialty();
        radiology.setDescription("Radiology");
        Specialty savedRadiology = specialtyService.save(radiology);

        Specialty surgery = new Specialty();
        surgery.setDescription("Surgery");
        Specialty savedSurgery = specialtyService.save(surgery);

        Specialty dentistry = new Specialty();
        dentistry.setDescription("Dentistry");
        Specialty savedDentistry = specialtyService.save(dentistry);

        Owner owner1 = new Owner();
        owner1.setFirstName("Jan");
        owner1.setLastName("Kowalski");
        owner1.setAddress("Poznanska 123");
        owner1.setCity("Kicin");
        owner1.setTelephone("123456789");

        Pet janPet = new Pet();
        janPet.setPetType(saveDogPetType);
        janPet.setOwner(owner1);
        janPet.setBirthDate(LocalDate.now());
        janPet.setName("Reksio");
        owner1.getPets().add(janPet);

        ownerService.save(owner1);

        Owner owner2 = new Owner();
        owner2.setFirstName("Piotr");
        owner2.setLastName("Nowak");
        owner2.setAddress("Dluga 32");
        owner2.setCity("Poznan");
        owner2.setTelephone("987654321");

        Pet piotrCat = new Pet();
        piotrCat.setPetType(saveCatPetType);
        piotrCat.setOwner(owner2);
        piotrCat.setBirthDate(LocalDate.now());
        piotrCat.setName("Meow");
        owner2.getPets().add(piotrCat);

        ownerService.save(owner2);

        Visit catVisit = new Visit();
        catVisit.setPet(piotrCat);
        catVisit.setDate(LocalDate.now());
        catVisit.setDescription("Limping");

        visitService.save(catVisit);

        System.out.println("Loaded owners...");

        Vet vet1 = new Vet();
        vet1.setFirstName("Anna");
        vet1.setLastName("Szafran");
        vet1.getSpecialities().add(savedRadiology);
        vet1.getSpecialities().add(savedDentistry);

        vetService.save(vet1);

        Vet vet2 = new Vet();
        vet2.setFirstName("Mateusz");
        vet2.setLastName("Kaczmarek");
        vet2.getSpecialities().add(savedSurgery);

        vetService.save(vet2);

        System.out.println("Loaded vets...");
    }
}
