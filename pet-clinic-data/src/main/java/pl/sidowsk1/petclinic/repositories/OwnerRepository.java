package pl.sidowsk1.petclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.sidowsk1.petclinic.model.Owner;

import java.util.List;

public interface OwnerRepository extends CrudRepository<Owner, Long> {

    Owner findByLastName(String lastName);

    List<Owner> findAllByLastNameLike(String lastName);
}
