package pl.sidowsk1.petclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.sidowsk1.petclinic.model.Specialty;

public interface SpecialtyRepository extends CrudRepository<Specialty, Long> {
}
