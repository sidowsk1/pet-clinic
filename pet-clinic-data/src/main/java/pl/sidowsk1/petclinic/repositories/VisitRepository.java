package pl.sidowsk1.petclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.sidowsk1.petclinic.model.Visit;

public interface VisitRepository extends CrudRepository<Visit, Long> {
}
