package pl.sidowsk1.petclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.sidowsk1.petclinic.model.Pet;

public interface PetRepository extends CrudRepository<Pet, Long> {
}
