package pl.sidowsk1.petclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.sidowsk1.petclinic.model.Vet;

public interface VetRepository extends CrudRepository<Vet, Long> {
}
