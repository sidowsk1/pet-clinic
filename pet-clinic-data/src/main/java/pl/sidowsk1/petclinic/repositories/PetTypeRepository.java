package pl.sidowsk1.petclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.sidowsk1.petclinic.model.PetType;

public interface PetTypeRepository extends CrudRepository<PetType, Long> {
}
