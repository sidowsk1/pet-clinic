package pl.sidowsk1.petclinic.services;

import pl.sidowsk1.petclinic.model.Pet;

public interface PetService extends CrudService<Pet, Long> {
}
