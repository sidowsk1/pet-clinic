package pl.sidowsk1.petclinic.services;

import pl.sidowsk1.petclinic.model.PetType;

public interface PetTypeService extends CrudService<PetType, Long> {
}
