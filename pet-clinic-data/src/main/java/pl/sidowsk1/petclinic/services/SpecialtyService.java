package pl.sidowsk1.petclinic.services;

import pl.sidowsk1.petclinic.model.Specialty;

public interface SpecialtyService extends CrudService<Specialty, Long> {
}
