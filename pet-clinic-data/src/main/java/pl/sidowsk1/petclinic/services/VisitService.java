package pl.sidowsk1.petclinic.services;

import pl.sidowsk1.petclinic.model.Visit;

public interface VisitService extends CrudService<Visit, Long> {
}
