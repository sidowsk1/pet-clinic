package pl.sidowsk1.petclinic.services;

import pl.sidowsk1.petclinic.model.Vet;

public interface VetService extends CrudService<Vet, Long>{
}
